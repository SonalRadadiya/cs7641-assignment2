# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 14:23:40 2017
@author: JTay
"""

import numpy as np

import sklearn.model_selection as ms
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
thyroid = pd.read_hdf('datasets.hdf','ww')
thyroidX = thyroid.drop('Class',1).copy().values
thyroidY = thyroid['Class'].copy().values

thyroid_trainX, thyroid_testX, thyroid_trainY, thyroid_testY = ms.train_test_split(thyroidX, thyroidY, test_size=0.3, random_state=0,stratify=thyroidY)

pipe = Pipeline([('Scale',StandardScaler())])

trainX = pipe.fit_transform(thyroid_trainX,thyroid_trainY)
trainY = np.atleast_2d(thyroid_trainY).T
testX = pipe.transform(thyroid_testX)
testY = np.atleast_2d(thyroid_testY).T
trainX, valX, trainY, valY = ms.train_test_split(trainX, trainY, test_size=0.2, random_state=1,stratify=trainY)
test = pd.DataFrame(np.hstack((testX,testY)))
train = pd.DataFrame(np.hstack((trainX,trainY)))
val = pd.DataFrame(np.hstack((valX,valY)))
test.to_csv('m_test.csv',index=False,header=False)
train.to_csv('m_train.csv',index=False,header=False)
val.to_csv('m_val.csv',index=False,header=False)
