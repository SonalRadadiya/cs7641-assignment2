# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

df_ww = pd.read_csv('./thyroid.dat', header=None)
df_ww.columns = ['Age', 'Sex', 'On_thyroxine', 'Query_on_thyroxine', 'On_antithyroid_medication', 'Sick', 'Pregnant', 'Thyroid_surgery', 'I131_treatment', 'Query_hypothyroid', 'Query_hyperthyroid',
                 'Lithium', 'Goitre', 'Tumor', 'Hypopituitary', 'Psych', 'TSH', 'T3', 'TT4', 'T4U', 'FTI', 'Class']

df_ww['Age'] = df_ww['Age'].astype(float)
df_ww['Sex'] = df_ww['Sex'].astype(float)
df_ww['On_thyroxine'] = df_ww['On_thyroxine'].astype(float)
df_ww['Query_on_thyroxine'] = df_ww['Query_on_thyroxine'].astype(float)
df_ww['On_antithyroid_medication'] = df_ww['On_antithyroid_medication'].astype(float)
df_ww['Sick'] = df_ww['Sick'].astype(float)
df_ww['Pregnant'] = df_ww['Pregnant'].astype(float)
df_ww['Thyroid_surgery'] = df_ww['Thyroid_surgery'].astype(float)
df_ww['I131_treatment'] = df_ww['I131_treatment'].astype(float)
df_ww['Query_hypothyroid'] = df_ww['Query_hypothyroid'].astype(float)
df_ww['Query_hyperthyroid'] = df_ww['Query_hyperthyroid'].astype(float)
df_ww['Lithium'] = df_ww['Lithium'].astype(float)
df_ww['Goitre'] = df_ww['Goitre'].astype(float)
df_ww['Tumor'] = df_ww['Tumor'].astype(float)
df_ww['Hypopituitary'] = df_ww['Hypopituitary'].astype(float)
df_ww['Psych'] = df_ww['Psych'].astype(float)
df_ww['TSH'] = df_ww['TSH'].astype(float)
df_ww['T3'] = df_ww['T3'].astype(float)
df_ww['TT4'] = df_ww['TT4'].astype(float)
df_ww['T4U'] = df_ww['T4U'].astype(float)
df_ww['FTI'] = df_ww['FTI'].astype(float)
df_ww['Class'] = df_ww['Class'].astype(float)

#put data into hdf file - this is a one-time step
df_ww.to_hdf('datasets.hdf', 'ww')
